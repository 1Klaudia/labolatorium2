import abc
import TicGame


class NotAProperNumberRange(Exception):
    pass


class NotFreeSpace(Exception):
    pass


class NotANumber(Exception):
    pass


class NotANumberinProperRange(Exception):
    pass


class AbstractValidator(object):
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def validate(self, input):
        """validate input of gamer move"""


class OptionValidator(AbstractValidator):

    def __init__(self, input):
        self.input = input

        self.list = [str(i) for i in range(101)]

    def validate1(self):
        if not self._properNumber():
            raise NotAProperNumberRange()

    def validate2(self, board):
        if not self._freeSpace(board):
            raise NotFreeSpace()

    def validate3(self):
        if not self._notANumberinProperRange():
            raise NotANumberinProperRange()

    def _properNumber(self):
        message = self.input in ['1', '2', '3', '4', '5', '6', '7', '8', '9']
        return message

    def _freeSpace(self, board):
        ticgame_object = TicGame.TicTacToeGame()

        board_index = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        message_list = []
        for i in board_index:
            if ticgame_object.is_space_free(board, i):
                message_list.append(str(i))

        message = self.input in message_list
        return message

    def _notANumberinProperRange(self):

            message = self.input in self.list
            print(message)
            return message





