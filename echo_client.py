import socket
import log_creator


class MyEchoClient:

    @log_creator.name_info
    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self.address = address
        self.port = port
        self.user_message = 'bum'

    @log_creator.name_info
    def send_msg(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (self.address, self.port)
        print('connecting to %s port %s' % server_address)
        sock.connect(server_address)
        sock.send(self.user_message.encode("utf8"))

        while self.user_message != 'x':
            data = sock.recv(self.data_size).decode("utf8")
            print(data)
            self.user_message = input("Choose option from menu")
            sock.send(self.user_message.encode("utf8"))
        sock.close()


if __name__ == "__main__":
    host = 'localhost'
    port = 50001
    data_size = 1024
    client = MyEchoClient(host, port, data_size)
    client.send_msg()
