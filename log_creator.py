import os
import logging

log_file = os.path.join(os.getenv('HOME'),'tic_tac_toe.log')
print('Tworzenie logu w' + log_file)


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s : %(levelname)s : %(message)s',
    filename=log_file,
    filemode='w',
)


def name_info(function):
    def pack(*args,**kwargs):
        logging.info('Wykonywanie funkcji '+function.__name__)
        return(function(*args, **kwargs))
    return pack
