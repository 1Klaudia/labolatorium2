import log_creator
import sys
import OptionValidator
from random import randint


class TicTacToeGame():

    @log_creator.name_info
    def __init__(self):
        self.board = [' '] * 9
        self.board_numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
        self.player1_marker = 'X'
        self.player2_marker = 'O'
        self.run = False
        self.win_configurations = (
            [6, 7, 8], [3, 4, 5], [0, 1, 2], [0, 3, 6], [1, 4, 7], [2, 5, 8],
            [0, 4, 8], [2, 4, 6],
            )

        self.board_draw = '''
           \t-------------
           \t| %s | %s | %s |
           \t-------------
           \t| %s | %s | %s |
           \t-------------
           \t| %s | %s | %s |
           \t--------------
           '''
        self.info = ''

    @log_creator.name_info
    def game_info(self):
        TicTacToeGame.print_board_numbers(self)
        return('This is a Tic Tac Toe game\n You can see the board with numbers form 1 to 9.\n \\'
            ' Your work is to choose number of cell when is your turn.\n Good luck\n'
                '*******************************\n')

    @log_creator.name_info
    def menu(self):
        return '11:play tic tac toe game\n' + '22:play more less game\n' + '33:TicTacToe info\n' + '44:More Less info\n' + '55:menu\n''x:exit\n'+'*************************'

    @log_creator.name_info
    def print_board_numbers(self):
        print('This is the board')
        print(self.board_draw % tuple(self.board_numbers[0:3] + self.board_numbers[3:6] + self.board_numbers[6:9]))

    @log_creator.name_info
    def print_board_durig_game(self, board):
        print(self.board_draw % tuple(board[0:3] + board[3:6] + board[6:9]))
        self.info += (self.board_draw % tuple(board[0:3] + board[3:6] + board[6:9]))
        return (self.board_draw % tuple(board[0:3] + board[3:6] + board[6:9]))

    @log_creator.name_info
    def player_move(self, board, board_index, player_marker):
        board[board_index] = player_marker
        return board

    @log_creator.name_info
    def is_space_free(self, board, index):
        return board[index] == ' '

    def get_player_move(self, move):

        if move == 'x':
            sys.exit()
        validator1 = OptionValidator.OptionValidator(move)
        validator1.validate1()
        move = int(move)-1
        validator2 = OptionValidator.OptionValidator(move)
        validator2.validate2(self.board)
        return move

    @log_creator.name_info
    def board_checker(self, board, marker):
        counter = 0
        for config in self.win_configurations:
            if (board[config[0]] == board[config[1]] == board[config[2]] == marker):
                print('and the winner is ' + marker)
                TicTacToeGame.quit_game(self)
        for index in range(9):

            if not TicTacToeGame.is_space_free(self, self.board, index):
                counter += 1
        if counter == 9:
            print('end of the game - full board ')

            TicTacToeGame.quit_game(self)

    @log_creator.name_info
    def quit_game(self):
        TicTacToeGame.print_board_durig_game(self, self.board)
        #print('koniec gry')
        sys.exit()

    @log_creator.name_info
    def main_game(self, move):
        self.run = True
        TicTacToeGame.game_info(self)

        while self.run:

            move_x = None
            move_o = None
            while move_x is None:
                try:
                    print('gracz 1:x')
                    move = input('Podaj pole')
                    move_x = TicTacToeGame.get_player_move(self, move)
                    TicTacToeGame.player_move(self, self.board, move_x, self.player1_marker)
                    TicTacToeGame.print_board_durig_game(self, self.board)
                    TicTacToeGame.board_checker(self, self.board, self.player1_marker)
                except OptionValidator.NotAProperNumberRange:
                    print('argumentem  musi byc liczba od 1 do 9  podaj jeszcze raz')
                except OptionValidator.NotFreeSpace:
                    print('pole zajete, wybierz inne')

            while move_o is None:
                try:
                    print('gracz 2:o podaje swój ruch')

                    move_o = randint(0, 8)
                    while not self.is_space_free(self.board, move_o):
                        move_o = randint(0, 8)
                    TicTacToeGame.player_move(self, self.board, move_o, self.player2_marker)
                    TicTacToeGame.print_board_durig_game(self, self.board)

                    TicTacToeGame.board_checker(self, self.board, self.player2_marker)
                except OptionValidator.NotAProperNumberRange:
                    print('argumentem  musi byc liczba od 1 do 9 podaj jeszcze raz')
                except OptionValidator.NotFreeSpace:
                        print('pole zajete, wybierz inne')


if __name__ == "__main__":
    object = TicTacToeGame()

    object.main_game(3)

