import log_creator
import sys
import OptionValidator
from random import randint


class MoreLessGame():

    @log_creator.name_info
    def __init__(self):
        self.run = False
        self.proper_numbers_board = []

    @log_creator.name_info
    def menu(self):
        return '11:play tic tac toe game\n' + '22:play more less game\n' + '33:TicTacToe info\n' + '44:More Less info\n' + '55:menu\n''x:exit\n'+'*************************'

    @log_creator.name_info
    def game_info(self):

        return ('This is a More Less  game\n '
              ' Your work is to find number from 1 to 100 I am thinking about.\n Good luck\n'
                      '*******************************\n')

    @log_creator.name_info
    def quit_game(self):
        print('The end')
        sys.exit()

    @log_creator.name_info
    def number_checker(self, number_to_guess, user_guess):
        for i in range(1, 101):
            self.proper_numbers_board.append(str(i))

        if user_guess == 'x':
            sys.exit()
        else:
            validator = OptionValidator.OptionValidator(user_guess)
            validator.validate3()
            int_guess = int(user_guess)

            if int_guess > number_to_guess:

                return ('Your guess is too high')

            elif int_guess < number_to_guess:

                return ('Your guess is too low')

            else:
                print ('Bingo! You won')
                MoreLessGame.quit_game(self)

    @log_creator.name_info
    def main_game(self, guess):

        self.run = True

        number_to_guess = randint(1, 100)

        while self.run:

            try:
                guess = input('Try to guess my number')

                print(MoreLessGame.number_checker(self, number_to_guess, guess))
            except OptionValidator.NotANumberinProperRange:
                print('You have to write a number in range (0, 100)')








