from unittest import TestCase
import unittest
import OptionValidator


class TestOptionValidator(TestCase):

    def test_should_raise_NotAProperNumberRangeException_when_letter(self):
        input = 'w'
        validator = OptionValidator.OptionValidator(input)
        self.assertRaises(OptionValidator.NotAProperNumberRange, validator.validate1)

    def test_should_raise_NotAProperNumberRangeException_when_wrong_number(self):
        input = '11'
        validator = OptionValidator.OptionValidator(input)
        self.assertRaises(OptionValidator.NotAProperNumberRange, validator.validate1)

    def test_should_test_proper_work(self):
        input = '2'
        validator = OptionValidator.OptionValidator(input)
        validator.validate1()

    def test_if_raise_notANumberinProperRange_when_wrong_number(self):
        input = '112'
        validator = OptionValidator.OptionValidator(input)
        self.assertRaises(OptionValidator.NotANumberinProperRange, validator.validate3)

    def test_if_raise_notANumberinProperRange_when_letter(self):
        input = 'w'
        validator = OptionValidator.OptionValidator(input)
        self.assertRaises(OptionValidator.NotANumberinProperRange, validator.validate3)

    def if_raise_NotFreeSpace_when_full_board(self):
        board = ['X', 'O', 'X', 'O', 'X', 'O', 'X', 'X', 'X']
        input = 1
        validator = OptionValidator.OptionValidator(input)
        self.assertRaises(OptionValidator.NotFreeSpace, validator.validate2(board))

    def if_work_properly_when_free_space(self):
        board = [' ', 'O', 'X', 'O', 'X', 'O', 'X', 'X', 'X']
        input = 1
        validator = OptionValidator.OptionValidator(input)
        validator.validate2(board)

if __name__ == '__main__':
    unittest.main()
