from unittest import TestCase
import unittest
import moreless
from unittest.mock import patch




class TestMoreLess(TestCase):


    def test_if_proper_string_when_low_number(self):

        moreless_object = moreless.MoreLessGame()

        expected_string = 'Your guess is too low'
        self.assertEqual(moreless_object.number_checker(11,'5'),expected_string )

    def test_if_proper_string_when_high_number(self):

        moreless_object = moreless.MoreLessGame()

        expected_string = 'Your guess is too high'
        self.assertEqual(moreless_object.number_checker(11,'55'),expected_string )

    @patch('moreless.MoreLessGame.quit_game', return_value=None)
    def test_if_end_game_when_player_won_and_give_proper_string(self, mock_value):

        moreless_object = moreless.MoreLessGame()


        moreless_object.number_checker(55,'55')
        self.assertTrue(mock_value.called)

























if __name__ == '__main__':
    unittest.main()
