from unittest import TestCase
import unittest
from unittest.mock import patch
from GamesMenager import Manager
from newTicTac import TicTacToeGame
from moreless import MoreLessGame


class TestGamesMenager(TestCase):

    def test_if_function_return_proper_statement_when_dont_choose_menu_option_properly(self):
        expected_statement = 'You have to choose option from menu\n'
        manager = Manager()
        current_statement = manager.choose_action('r')
        self.assertEqual(current_statement, expected_statement)

    def test_if_function_print_menu_at_the_beggining(self):
        beggin_message =  '11:play tic tac toe game\n' + '22:play more less game\n' + '33:TicTacToe info\n' + '44:More Less info\n' + '55:menu\n''x:exit\n'+'*************************'
        manager = Manager()
        current_statement = manager.choose_action('bum')
        self.assertEqual(current_statement, beggin_message)

    def test_if_function_pront_info_about_tictactoe(self):
        manager = Manager()
        info_message = ('This is a Tic Tac Toe game\n You can see the board with numbers form 1 to 9.\n \\'
            ' Your work is to choose number of cell when is your turn.\n Good luck\n'
                '*******************************\n')
        current_statement = manager.choose_action('33')
        self.assertEqual(current_statement, info_message)

    def test_if_function_pront_info_about_moreless(self):
        manager = Manager()
        info_message = ('This is a More Less  game\n '
              ' Your work is to find number from 1 to 100 I am thinking about.\n Good luck\n'
                      '*******************************\n')
        current_statement = manager.choose_action('44')
        self.assertEqual(current_statement, info_message)

#  def test_if_function_open_tictactoe_game(self):
#        manager = Manager()
#        game = TicTacToeGame()
#        expected_statement = game.main_game(2)
#        current_statement = manager.check_command('11')
#        self.assertEqual(current_statement, expected_statement)




if __name__ == '__main__':
    unittest.main()