from unittest import TestCase
import unittest
from unittest.mock import patch
import newTicTac


class TestTicGame(TestCase):

    def test_board_drawing_during_the__main_game(self):
        board = ['X', 'O', 'X', 'O', 'X', 'O', 'X', ' ', ' ']
        expected_board_draw = '''
           \t-------------
           \t| X | O | X |
           \t-------------
           \t| O | X | O |
           \t-------------
           \t| X |   |   |
           \t--------------
           '''

        tic_object = newTicTac.TicTacToeGame()
        program_board = tic_object.print_board_durig_game(board)
        self.assertEqual(program_board, expected_board_draw)

    @patch('newTicTac.TicTacToeGame.quit_game', return_value=None)
    def test_game_loop_end_when_winner(self, mock_value1):

        board = ['X', 'X', 'X', 'O', ' ', 'O', ' ', ' ', 'O']
        tic_object = newTicTac.TicTacToeGame()
        tic_object.board_checker(board, 'X')
        self.assertTrue(mock_value1.called)

    @patch('newTicTac.TicTacToeGame.quit_game', return_value=None)
    def test_game_loop_not_end_when__not_winner(self, mock_value1):

        board = ['X', 'X', 'X', 'O', ' ', 'O', ' ', ' ', 'O']
        tic_object = newTicTac.TicTacToeGame()
        tic_object.board_checker(board, 'O')
        self.assertFalse(mock_value1.called)

    @patch('newTicTac.TicTacToeGame.quit_game', return_value=None)
    def test_game_loop_end_when_full_board(self, mock_value1):

        board = ['X', 'X', 'O', 'O', 'X', 'X', 'X', 'O', 'O']
        tic_object = newTicTac.TicTacToeGame()
        tic_object.board_checker(board, 'X')
        for i in range(9):
            self.assertEqual(tic_object.is_space_free(board, i), False)


if __name__ == '__main__':
    unittest.main()
