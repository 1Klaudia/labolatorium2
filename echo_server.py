import socket
from GamesMenager import Manager
from newTicTac import TicTacToeGame


class MyEchoServer:

    def __init__(self, address, port, data_size):
        self.data_size = data_size
        self.port = port
        self.address = address
        self.game_manager = Manager()
        self.tic = TicTacToeGame
        self.data = '55'
        self.game = TicTacToeGame()

    def handle_connection(self):
        while self.data != 'x':
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            server_address = (self.address, self.port)
            print ('bind to %s port %s' % server_address)
            sock.bind(server_address)

            sock.listen(1)
            client1, client_address = sock.accept()

            while self.data != 'x':

                print(self.data)
                self.data = client1.recv(self.data_size).decode("utf8")
                returned_data = self.game_manager.choose_action(self.data)
                if self.data:
                    client1.send(returned_data.encode("utf8"))
            client1.close()


if __name__ == "__main__":
    host = 'localhost'
    port = 50001
    data_size = 1024
    server = MyEchoServer(host, port, data_size)
    server.handle_connection()
