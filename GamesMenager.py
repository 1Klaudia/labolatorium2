from newTicTac import TicTacToeGame
import log_creator
from moreless import MoreLessGame


class Manager:

    @log_creator.name_info
    def __init__(self):
        self.game = TicTacToeGame()
        self.user_orders = {}
        self.user_orders['play_tic_tac_game'] = '11'
        self.user_orders['play_more_less_game'] = '22'
        self.user_orders['game_tictac_info'] = '33'
        self.user_orders['game_moreless_info'] = '44'
        self.user_orders['menu'] = '55'
        self.user_orders['exit'] = 'x'

    @log_creator.name_info
    def choose_action(self, order):

        if order == 'bum':
            returned_value = self.game.menu()

        elif order == self.user_orders['play_tic_tac_game']:
            print('You are playing tictactoe')
            self.game = TicTacToeGame()
            returned_value = self.game.main_game(order)
        elif order == self.user_orders['play_more_less_game']:
            print('You are playing moreless')
            self.game = MoreLessGame()
            returned_value = self.game.main_game(order)

        elif order == self.user_orders['menu']:
            returned_value = self.game.menu()

        elif order == self.user_orders['game_tictac_info']:
            game = TicTacToeGame()
            returned_value = game.game_info()

        elif order == self.user_orders['game_moreless_info']:
            game = MoreLessGame()
            returned_value = game.game_info()

        elif order == self.user_orders['exit']:
            returned_value = self.game.quit_game()

        else:
            returned_value = 'You have to choose option from menu\n'
        return returned_value
